#include <iostream>
#include <iomanip>
#include <vector>

#include <assert.h>
using namespace std;

double sigm(double x) {
	return 2 / (1 + exp(-5 * x)) - 1;
	//	return x > 0 ? 1 : -1;
}

class Vector;
class Matrix;
std::ostream &operator<<(std::ostream &os, const Vector &v);
std::ostream &operator<<(std::ostream &os, const Matrix &v);

struct Vector {
	int m;
	double* d;
  
  Vector(): m(0), d(0) {
    cout << "Vector()" << *this << endl << flush;
  }

	Vector(int m) : m(m) {
		d = new double[m];
		for (int i = 0; i < m; i++)
			d[i] = 0;
    
    cout << "Vector(int)" << *this << endl << flush;
	}

	Vector(int m, double* a) : m(m) {
		d = new double[m];
		for (int i = 0; i < m; i++)
			d[i] = a[i];
    
    cout << "Vector(int, double *)" << *this << " orig data:" << a << endl << flush;
	}
  
  Vector& operator= (const Vector& v) {
    //resize?
    if(m != v.m) {
      cout << "Vector delete for resize:" << *this << endl << flush;
      delete[] d;
      m = v.m;
      d = new double[m];
    }
    for (int i = 0; i < m; i++)
			d[i] = v.d[i];
    
    cout << "Vector::operator=(const Vector &)" << *this << " orig data: " << v.d << endl << flush;
    return *this;
  }

	Vector(const Vector &v) : Vector(v.m, v.d) {}

	~Vector() {
    cout << "~Vector()" << *this << endl << flush;
    assert(m > 0 && d != 0);
    m = -1;
    delete[] d;
    cout << "~Vector() finished" << endl << flush;
	}

	double operator [](int i) const {
		assert(0 <= i && i < m);
		return d[i];
	}

	void print() const {
		for (int i = 0; i < m; i++)
			cout << d[i] << ' ';
		cout << endl;
	}

	void apply_sigmoid() {
		for (int i = 0; i < m; i++)
			d[i] = sigm(d[i]);
	}
  
  friend std::ostream &operator<<(std::ostream &os, const Vector &v);
};

std::ostream &operator<<(std::ostream &os, const Vector &v) { 
  return os << "Vector(d=" << v.d << ", m=" << v.m << ")";
}


struct Matrix {
	int n, m;
	double** d;

	Matrix(int n, int m) : n(n), m(m) {
    d = new double*[n];
		for (int i = 0; i < n; i++) {
			d[i] = new double[m];
		}
    cout << "Matrix(int,int): " << *this << " size: " << m << endl << flush;
	}
  
  Matrix(const Matrix &a) : Matrix(a.n, a.m) {
    cout << "Matrix(const Matrix &a): " << *this << " from orig data: " << a.d << endl << flush;
    for(int i = 0; i < n; ++i) {
      for(int j = 0; j < m; ++j) {
        d[i][j] = a.d[i][j];
      }
    }
  }
  
  Matrix& operator= (const Matrix& v)
  {
    //resize?
    if(n != v.n || m != v.m) {
      cout << "Matrix delete for resize: " << *this << endl << flush; 
      for (int i = 0; i < n; i++) {
        delete[] d[i];
      }
      delete[] d;
      
      n = v.m;
      m = v.m;
      d = new double*[n];
      for (int i = 0; i < n; i++) {
        d[i] = new double[m];
      }
    }
    
    for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				d[i][j] = v.d[i][j];
    
    cout << "Matrix::operator=(const Matrix &): " << *this << " orig data: " << v.d << endl << flush;
    return *this;
  }

	~Matrix() {
    cout << "~Matrix(): " << *this << endl << flush;
    
    for (int i = 0; i < n; i++) {
    	delete[] d[i];
    }
    delete[] d;
	}

	void init(double* a) {
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				d[i][j] = a[i * m + j];
	}

	Vector operator * (const Vector &v) const {
		assert(v.m + 1 == m);
    Vector res = Vector(n);
		for (int i = 0; i < n; i++) {
			res.d[i] = d[i][0];
			for (int j = 1; j < m; j++) {
				res.d[i] += d[i][j] * v.d[j - 1];
			}
		}
		return res;
	}

	void print() {
		cout << n << ' ' << m << endl;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++)
				cout << d[i][j] << ' ';
			cout << endl;
		}
	}
  
  friend std::ostream &operator<<(std::ostream &os, Matrix const &m);
};

std::ostream &operator<<(std::ostream &os, Matrix const &v) { 
  return os << "Matrix(" << v.d << " size: " << v.n << " by " << v.m << ")";
}

struct My_RNN {
	int s11, s12, s21, s22;
	Vector x;
  Vector last;
	Matrix T1;
	Matrix T2;

	My_RNN(int s11, int s12, int s21, int s22, double* coeff1, double* coeff2)
			: s11(s11), s12(s12), s21(s21), s22(s22), x(s12 - 1), last(s21), T1(s11, s12), T2(s21, s22) {
		T1.init(coeff1);
		T2.init(coeff2);
	}

	void reset() {
		for (int i = 0; i <= s11; i++)
			x.d[i] = -1;
	}

	Vector step(int l, int* d) {
    cout << "step begin" << flush <<  endl;
		for (int i = 0; i < l; i++)
			x.d[i] = d[i];

		cout << "multiply 1st: " << flush <<  endl;
		Vector y(T1 * x);
		y.apply_sigmoid();

    cout << "multiply 2nd: " << flush <<  endl;
		Vector x(T2 * y);
    
    cout << "assign last: " << flush <<  endl;
		last = T2 * y;

    cout << "manual assign x: " << flush <<  endl;
    assert(x.m >= s11);
    
		for (int i = 1; i < s11; i++) {
      x.d[i] = y[i - 1];
    }

    cout << "step end" << flush <<  endl;
		return last;
	}

	Vector get_last() {
		return Vector(last);
	}
};