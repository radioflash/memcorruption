#include <iostream>
#include <vector>
#include <random>
#include <fstream>
#include <math.h>
#include <algorithm>

#include "rnn_lib.h"
using namespace std;

const int S11 = 16, S12 = 37, S21 = 16, S22 = 17;
const int M = 10;

int q;
vector < pair < int, int > > result;
vector < My_RNN > nn;

vector < int > c1, c2;


void initialize() {
	for (int i = 0; i < 3; i++) {
		c1.push_back(i);
		c2.push_back(i);
	}
}

void initial_step(int* d, int id) {
	assert(c1.size() == 3 && c2.size() == 3);

	for (int i = 0; i < 20; i++)
		d[i] = 0;
	d[0] = 1; // initial
	d[3 + c1[id]] = 1; // card 1
	d[6 + c2[id]] = 1; // card 2
	d[9] = 1; // x answers
	d[11] = 1; // accept card 1
	d[12] = 1; // accept card 2
}

void create_response(int* d, int cc1, int cc2, int res_c, int req, int res) {
	assert(0 <= cc1 && cc1 <= 2 && 0 <= cc2 && cc2 <= 2 && 0 <= res_c && res_c <= 2);

	for (int i = 0; i < 20; i++)
		d[i] = -1;

	d[req] = 1; // request
	d[3 + cc1] = 1; // card 1
	d[6 + cc2] = 1; // card 2
	d[8 + res] = 1; // response
	d[10 + res_c] = 1; // card 1 / 2
}

int play(int a, int b) {
	nn[a].reset();
	nn[b].reset();

	assert(c1.size() == 3 && c2.size() == 3);
	random_shuffle(c1.begin(), c1.end());
	random_shuffle(c2.begin(), c2.end());
	assert(c1.size() == 3 && c2.size() == 3);

	int* d = new int[20];
  cout << "init step 0 " << endl << std::flush;
	initial_step(d, 0);
	nn[a].step(20, d);
  
  cout << "init step 1 " << endl << std::flush;
	initial_step(d, 1);
	nn[b].step(20, d);
  
  cout << "init steps finished " << endl << std::flush;

	for (int q = 0, t = 0; q < 6; q++, t = t ^ 1) {
		Vector trn = nn[t ? b : a].get_last();

		// ask
		int cc1 = 0;
		for (int i = 1; i <= 3; i++)
			if (trn.d[i] > trn.d[1 + cc1])
				cc1 = i - 1;
		int cc2 = 0;
		for (int i = 4; i <= 6; i++)
			if (trn.d[i] > trn.d[4 + cc2])
				cc2 = i - 4;

		int g = t ? 0 : 1; // enemy
		int* d_req = new int[20];
		int* d_res = new int[20];

		if (c1.size() != 3 || c2.size() != 3) {
			cout << a << ' ' << b << endl;
			exit(0);
		}

		assert(c1.size() == 3 && c2.size() == 3);
		if (c1[g] == cc1 && c2[g] == cc2) {
      //cout << "copy gl " << endl << std::flush;
			Vector gl = nn[g].get_last();
			if (gl.d[14] > gl.d[15]) {
				create_response(d_req, cc1, cc2, 1, 1, 2);
				create_response(d_res, cc1, cc2, 1, 2, 1);
			} else {
				create_response(d_req, cc1, cc2, 2, 1, 2);
				create_response(d_res, cc1, cc2, 2, 2, 1);
			}
		} else if (c1[g] == cc1) {
			create_response(d_req, cc1, cc2, 1, 1, 2);
			create_response(d_req, cc1, cc2, 1, 2, 1);
		} else if (c2[g] == cc2) {
			create_response(d_req, cc1, cc2, 2, 1, 2);
			create_response(d_req, cc1, cc2, 2, 2, 1);
		} else {
			create_response(d_req, cc1, cc2, 0, 1, 2);
			create_response(d_req, cc1, cc2, 0, 2, 1);
		}

    cout << "step 1st " << endl << std::flush;
		nn[t ? b : a].step(20, d_req);
    
    cout << "step 2nd " << endl << std::flush;
		nn[t ? a : b].step(20, d_res);

    cout << "assign trn " << endl << std::flush;
		trn = nn[t ? b : a].get_last();

		if (trn.d[7] > 0) {
			// try
			int cc1 = 0;
			for (int i = 8; i <= 10; i++)
				if (trn.d[i] > trn.d[8 + cc1])
					cc1 = i - 8;
			int cc2 = 0;
			for (int i = 11; i <= 13; i++)
				if (trn.d[i] > trn.d[11 + cc2])
					cc2 = i - 11;

			assert(c1.size() == 3 && c2.size() == 3);
			if (c1[2] == cc1 && c2[2] == cc2)
				return t ? -1 : 1;
			else
				return t ? 1 : -1;
		}
	}

	return 0;
}

int main() {
	initialize();

	double* coeff1 = new double[S11 * S12];
	double* coeff2 = new double[S21 * S22];

	ifstream fin;
	fin.open("coeff");
	int p;
	fin >> p;
	result.reserve(p);
	nn.reserve(p);
	for (int k = 0; k < p; k++) {
		// read coeffs
		for (int i = 0; i < S11 * S12; i++)
			fin >> coeff1[i];
		for (int i = 0; i < S21 * S22; i++)
			fin >> coeff2[i];

		// init
		nn.push_back(My_RNN(S11, S12, S21, S22, coeff1, coeff2));
		result.push_back(make_pair(0, 0));
	}
	fin.close();

	for (int i = 0; i < p; i++) {
		// cout << i << endl;
		//	 	for (int g = 0; g < M; g++) {
		for (int j = 0; j < p; j++) {
			//	 	 	int j = rand() % p;
			//	 	 	while (i == j)
			//	 	 		j = rand() % p;
			if (i == j)
				continue;

			// best of 3
			int w = 0;
			for (int k = 0; k < 3; k++)
				w += play(i, j);
			/*
			if (w != 0) {
			cout << i << ' '<< j << endl;
			}
			*/
			if (w > 0)
				result[i].first++;
			else if (w < 0)
				result[j].first++;
			result[i].second++;
			result[j].second++;
		}
	}

	ofstream fout("evaluation");
	for (int i = 0; i < p; i++) {
		fout << 1. * result[i].first / result[i].second << endl;
	}
	fout.close();

	return 0;
}